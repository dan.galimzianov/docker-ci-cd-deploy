const express = require('express');

const app = express();

app.use('/health', (req, res) => {
    res.end('watchtower test!!!')
});

app.listen(5001, () => {
    console.log(`Server listen port: ${5001}`);
})
// rsync -e "ssh -i node-js-app" --exclude=node_modules -r ./WebstormProjects/node-example-app d-galimzianov@158.160.16.204:~/
// pm2 - очень просто дешево запустить приложение