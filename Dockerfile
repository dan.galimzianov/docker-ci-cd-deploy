FROM node:18

# создание директории приложения
# установка зависимостей
# символ астериск ("*") используется для того чтобы по возможности
# скопировать оба файла: package.json и package-lock.json
COPY package*.json ./
RUN npm install
COPY . .

USER node
EXPOSE 5000
CMD [ "node", "index.js" ]